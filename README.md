# Random Choice Picker

Uses randomness. I like random activity in my applications.

This is part of a course by [Brad Traversy and Florin Pop](https://github.com/bradtraversy/50projects50days).
